from .models import Book
from .serializers import BookSerializer
from rest_framework import generics


class BookListCreate(generics.ListCreateAPIView):
    queryset = Book.objects.all()
    serializer_class = BookSerializer



# from django.shortcuts import render
# from django.http import HttpResponse
#
# from .models import Greeting
#
# # Create your views here.
# def index(request):
#     # return HttpResponse('Hello from Python!')
#     return render(request, "index.html")
#
#
# def db(request):
#
#     greeting = Greeting()
#     greeting.save()
#
#     greetings = Greeting.objects.all()
#
#     return render(request, "db.html", {"greetings": greetings})