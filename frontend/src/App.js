import React, { Component } from 'react';
import { render } from "react-dom";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      loaded: false,
      placeholder: "Loading"
    };
  }

  componentDidMount() {
    fetch("api/book")
      .then(response => {
        if (response.status > 400) {
          return this.setState(() => {
            return { placeholder: "Something went wrong!" };
          });
        }
        return response.json();
      })
      .then(data => {
        this.setState(() => {
          return {
            data,
            loaded: true
          };
        });
      });
  }

  render() {
    return (<div>
                 <div className="searchForm">
                     <form className="example">
                        <input type="text" id="filter" placeholder="Книгу хочу очень.."/>
                        <button type="submit"><i className="fa fa-search"></i></button>
                    </form>
                </div>
                <div>
                <table>
                   <tr>
                     {this.state.data.map(contact => {
                      return (
                        <li key={contact.id}>
                          {contact.name} - {contact.email}
                        </li>
                      );
                    })}
                    </tr>
              </table>
             </div>
     </div>
    );
  }
}

export default App;

const container = document.getElementById("app");
render(<App />, container);